class Submission():

    q_id = 0
    s_text = ''
    s_user = ''

    def __init__(self, q_id, s_text, s_user):
        self.q_id = q_id
        self.s_text = s_text
        self.s_user = s_user

    def getQuestionID(self):
        return self.q_id

    def getSubmissionText(self):
        return self.s_text

    def getSubmissionUser(self):
        return self.s_user

