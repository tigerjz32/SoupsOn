from bishop.data_access.sql.SqlSproc import SqlSproc
from bishop.data_access.sql.SqlConnection import SqlConnection
from bishop.domain.sql.SqlAuthenticationType import SqlAuthenticationType
from bishop.domain.mapper_generic.ResultType import ResultType

class Connect(SqlSproc):
    def __init__(self, sproc, params):

        # create connection object
        connection = SqlConnection(SqlAuthenticationType.Windows, 'DATABASE')

        # add sproc parameters (may need to update the dates)
        params = params

        # call the parent constructor
        SqlSproc.__init__(self,
                          connection=connection,
                          database='SoupsOn',
                          schema='dbo',
                          sproc=sproc,
                          result_type=ResultType.DataFrame,
                          **params)