USE [master]
GO
/****** Object:  Database [SoupsOn]    Script Date: 7/29/2016 4:34:36 PM ******/
CREATE DATABASE [SoupsOn]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'SoupsOn', FILENAME = N'XX:\SoupsOn.mdf' , SIZE = 4096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 262144KB )
 LOG ON 
( NAME = N'SoupsOn_log', FILENAME = N'XX:\SoupsOn_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
 COLLATE SQL_Latin1_General_CP1_CI_AS
GO
ALTER DATABASE [SoupsOn] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [SoupsOn].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [SoupsOn] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [SoupsOn] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [SoupsOn] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [SoupsOn] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [SoupsOn] SET ARITHABORT OFF 
GO
ALTER DATABASE [SoupsOn] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [SoupsOn] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [SoupsOn] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [SoupsOn] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [SoupsOn] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [SoupsOn] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [SoupsOn] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [SoupsOn] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [SoupsOn] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [SoupsOn] SET  DISABLE_BROKER 
GO
ALTER DATABASE [SoupsOn] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [SoupsOn] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [SoupsOn] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [SoupsOn] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [SoupsOn] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [SoupsOn] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [SoupsOn] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [SoupsOn] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [SoupsOn] SET  MULTI_USER 
GO
ALTER DATABASE [SoupsOn] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [SoupsOn] SET DB_CHAINING OFF 
GO
ALTER DATABASE [SoupsOn] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [SoupsOn] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [SoupsOn] SET DELAYED_DURABILITY = DISABLED 
GO
USE [SoupsOn]
GO
/****** Object:  User [soupson]    Script Date: 7/29/2016 4:34:36 PM ******/
CREATE USER [soupson] FOR LOGIN [soupson] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_datareader] ADD MEMBER [soupson]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [soupson]
GO
GRANT CONNECT TO [soupson] AS [dbo]
GO
GRANT EXECUTE TO [soupson] AS [dbo]
GO
/****** Object:  UserDefinedFunction [dbo].[fSubmissionsIsValid]    Script Date: 7/29/2016 4:34:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[fSubmissionsIsValid](@question_id INT, @response INT)
RETURNS BIT
AS
BEGIN	
	DECLARE @qtype VARCHAR(16)
	SELECT @qtype  = question_type FROM questions WHERE question_id = @question_id

	RETURN 
		CASE 
			WHEN @qtype = 'text' AND EXISTS(SELECT 1 FROM answers WHERE question_id = @question_id AND answer_id = @response) THEN 1
			WHEN @qtype = 'rate' AND @response BETWEEN 1 AND 5 THEN 1
			WHEN @qtype = 'yn' AND @response IN (0,1) THEN 1
			ELSE 0 END
END


GO
/****** Object:  Table [dbo].[answers]    Script Date: 7/29/2016 4:34:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[answers](
	[answer_id] [int] IDENTITY(1,1) NOT NULL,
	[question_id] [int] NOT NULL,
	[answer] [varchar](512) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[image] [varchar](2048) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_answers] PRIMARY KEY CLUSTERED 
(
	[answer_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[question_set_questions]    Script Date: 7/29/2016 4:34:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[question_set_questions](
	[question_set_id] [int] NOT NULL,
	[question_id] [int] NOT NULL,
 CONSTRAINT [PK_question_set_questions] PRIMARY KEY CLUSTERED 
(
	[question_set_id] ASC,
	[question_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[question_sets]    Script Date: 7/29/2016 4:34:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[question_sets](
	[question_set_id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[is_active] [bit] NOT NULL,
 CONSTRAINT [PK_question_sets] PRIMARY KEY CLUSTERED 
(
	[question_set_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[questions]    Script Date: 7/29/2016 4:34:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[questions](
	[question_id] [int] IDENTITY(1,1) NOT NULL,
	[question] [varchar](512) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[question_type] [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
 CONSTRAINT [PK_Questions] PRIMARY KEY CLUSTERED 
(
	[question_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[submissions]    Script Date: 7/29/2016 4:34:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[submissions](
	[submission_id] [int] IDENTITY(1,1) NOT NULL,
	[question_id] [int] NOT NULL,
	[response] [int] NOT NULL,
	[user_id] [int] NULL,
	[dt] [datetime] NOT NULL,
 CONSTRAINT [PK_submissions] PRIMARY KEY CLUSTERED 
(
	[submission_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_answers]    Script Date: 7/29/2016 4:34:37 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_answers] ON [dbo].[answers]
(
	[question_id] ASC,
	[answer] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[answers]  WITH CHECK ADD  CONSTRAINT [FK_answers_Questions] FOREIGN KEY([question_id])
REFERENCES [dbo].[questions] ([question_id])
GO
ALTER TABLE [dbo].[answers] CHECK CONSTRAINT [FK_answers_Questions]
GO
ALTER TABLE [dbo].[question_set_questions]  WITH CHECK ADD  CONSTRAINT [FK_question_set_questions_question_sets] FOREIGN KEY([question_set_id])
REFERENCES [dbo].[question_sets] ([question_set_id])
GO
ALTER TABLE [dbo].[question_set_questions] CHECK CONSTRAINT [FK_question_set_questions_question_sets]
GO
ALTER TABLE [dbo].[question_set_questions]  WITH CHECK ADD  CONSTRAINT [FK_question_set_questions_questions] FOREIGN KEY([question_id])
REFERENCES [dbo].[questions] ([question_id])
GO
ALTER TABLE [dbo].[question_set_questions] CHECK CONSTRAINT [FK_question_set_questions_questions]
GO
ALTER TABLE [dbo].[submissions]  WITH CHECK ADD  CONSTRAINT [FK_submissions_Questions] FOREIGN KEY([question_id])
REFERENCES [dbo].[questions] ([question_id])
GO
ALTER TABLE [dbo].[submissions] CHECK CONSTRAINT [FK_submissions_Questions]
GO
ALTER TABLE [dbo].[submissions]  WITH CHECK ADD  CONSTRAINT [chkValidSubmissions] CHECK  (([dbo].[fSubmissionsIsValid]([question_id],[response])=(1)))
GO
ALTER TABLE [dbo].[submissions] CHECK CONSTRAINT [chkValidSubmissions]
GO
/****** Object:  StoredProcedure [dbo].[usp_get_answers]    Script Date: 7/29/2016 4:34:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_get_answers] @question_id INT = NULL
AS
SELECT a.question_id, a.answer_id, answer, image,
	result_prct = CONVERT(INT,
			(SELECT COUNT(1) FROM submissions WHERE question_id = q.question_id AND response = a.answer_id) * 100. / 
			(SELECT COUNT(1) FROM submissions WHERE question_id = q.question_id ))
FROM answers a
INNER JOIN question_set_questions qsq ON a.question_id = qsq.question_id
INNER JOIN question_sets qs ON qs.question_set_id = qsq.question_set_id
INNER JOIN questions q ON a.question_id = q.question_id
WHERE q.question_type = 'text'
	AND qs.is_active = 1 AND a.question_id = COALESCE(@question_id, a.question_id)
UNION ALL
SELECT q.question_id, nums.ans, CONVERT(VARCHAR,nums.ans), 
	img = CASE WHEN q.question_type = 'rate' THEN 'http://pngimg.com/upload/star_PNG1579.png'
				WHEN q.question_type = 'yn' AND ans = 0 THEN 'https://upload.wikimedia.org/wikipedia/commons/b/b1/Not_facebook_not_like_thumbs_down.png'
				WHEN q.question_type = 'yn' AND ans = 1 THEN 'https://upload.wikimedia.org/wikipedia/commons/1/13/Facebook_like_thumb.png'
				ELSE '' END,
	result_prct = CONVERT(INT,
			(SELECT COUNT(1) FROM submissions WHERE question_id = q.question_id AND response = nums.ans) * 100. / 
			(SELECT COUNT(1) FROM submissions WHERE question_id = q.question_id ))
FROM questions q
INNER JOIN (VALUES(0),(1),(2),(3),(4),(5),(6))nums(ans) 
	ON (q.question_type = 'rate' AND nums.ans BETWEEN 1 AND 6)
	OR (q.question_type = 'yn' AND nums.ans BETWEEN 0 AND 1)
INNER JOIN question_set_questions qsq ON q.question_id = qsq.question_id
INNER JOIN question_sets qs ON qs.question_set_id = qsq.question_set_id
WHERE qs.is_active = 1 AND q.question_id = COALESCE(@question_id, q.question_id)

GO
/****** Object:  StoredProcedure [dbo].[usp_get_question_set]    Script Date: 7/29/2016 4:34:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_get_question_set] @question_set_id INT

AS
SELECT q.question_id,
	q.question,
	q.question_type
FROM [questions] q
INNER JOIN question_set_questions qsq ON q.question_id = qsq.question_id
INNER JOIN question_sets qs ON qs.question_set_id = qsq.question_set_id
WHERE qs.question_set_id = @question_set_id;
GO
/****** Object:  StoredProcedure [dbo].[usp_get_question_set_active]    Script Date: 7/29/2016 4:34:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_get_question_set_active]
AS

SELECT s.question_set_id, s.name
FROM question_sets s
WHERE s.is_active = 1
ORDER BY s.name


GO
/****** Object:  StoredProcedure [dbo].[usp_submit_response]    Script Date: 7/29/2016 4:34:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_submit_response]
	@question_id INT,
	@response INT
AS

SET NOCOUNT ON

INSERT INTO submissions
VALUES(@question_id, @response, NULL, GETDATE())

SELECT * FROM submissions






GO
USE [master]
GO
ALTER DATABASE [SoupsOn] SET  READ_WRITE 
GO
