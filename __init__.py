from flask import Flask, render_template, request, redirect, url_for
from bishop.data_access.sql.SqlSproc import SqlSproc
from bishop.data_access.sql.SqlConnection import SqlConnection
from bishop.domain.sql.SqlAuthenticationType import SqlAuthenticationType
from bishop.domain.mapper_generic.ResultType import ResultType
from Connect import Connect
from Question import Question
from Submission import Submission

app = Flask(__name__)

@app.route('/')
def index():
    my_mapper = Connect('usp_get_question_set_active', {})
    question_sets = my_mapper.extract(ResultType.DataFrame)

    return render_template('index.html', num_of_questions=len(question_sets['question_set_id']), q_IDs=question_sets['question_set_id'], q_Texts=question_sets['name'])

@app.route('/<qs_ID>/')
def question_set(qs_ID):
    questions = []
    q_IDs = []
    q_Texts = []
    q_Types = []
    q_num_of_answers = []
    a_IDs = []
    a_Texts = []
    a_Images = []

    my_mapper = Connect('usp_get_question_set', {'question_set_id': qs_ID})
    df = my_mapper.extract(ResultType.DataFrame)

    for counter in range(len(df['question'])):
        my_mapper = Connect('usp_get_answers', {'question_id': df['question_id'][counter]})
        ansList = my_mapper.extract(ResultType.DataFrame)
        questions.append(Question(df['question_id'][counter], df['question'][counter], df['question_type'][counter], ansList['answer_id'], ansList['answer'], ansList['image']))

    for q in questions:
        q_IDs.append(q.getQuestionID())
        q_Texts.append(q.getQuestionText())
        q_Types.append(q.getQuestionType())
        a_IDs.append(q.getAnswerIDs())
        a_Texts.append(q.getAnswerTexts())
        a_Images.append(q.getAnswerImages())
        q_num_of_answers.append(len(q.getAnswerIDs()))

    return render_template('template.html', num_of_questions=len(q_IDs), num_of_answers=q_num_of_answers, q_IDs=q_IDs, q_Texts=q_Texts, q_Types=q_Types, a_IDs=a_IDs, a_Texts=a_Texts, a_Images=a_Images)

@app.route('/<qs_ID>/submit', methods=['POST'])
def submit(qs_ID):
    submissions = []
    try:
        for n in range(int(request.form['num_of_questions'])):
            string_a = str(n+1) + '_a_value'
            string_q = str(n+1) + '_q_value'
            submissions.append(Submission(request.form[string_q], request.form[string_a], ''))
        submit_to_db(submissions)
    except:
        print("Error when fetching data from form!")
        raise

    return redirect(url_for('question_set', qs_ID=qs_ID, alert='success'))

def submit_to_db(submissions):
    conn = SqlConnection(SqlAuthenticationType.Windows, 'DATABASE');
    with conn.get_open_connection() as cnxn:
        with cnxn.cursor() as cursor:
            try:
                try:
                    for s in submissions:
                        cursor.execute('EXEC SoupsOn.dbo.usp_submit_response @question_id = %(question_id)s, @response = %(response)s' % {'question_id': s.getQuestionID(), 'response': s.getSubmissionText()}) # change
                        cnxn.commit()
                except IOError:
                    return 1
                return 0
            except IOError:
                return 1

if __name__ == '__main__':
    app.run(host='0.0.0.0')


