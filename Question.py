class Question():
	
	q_ID = 0
	q_Text = ''
	q_Type = ''
	a_IDs = []
	a_Texts = []
	a_Images = []

	def __init__(self, q_ID, q_Text, q_Type, a_IDs, a_Texts, a_Images):
		self.q_ID = q_ID
		self.q_Text = q_Text
		self.q_Type = q_Type
		self.a_IDs = a_IDs
		self.a_Texts = a_Texts
		self.a_Images = a_Images
	
	def getQuestionID(self):
		return self.q_ID

	def getQuestionText(self):
		return self.q_Text
	
	def getQuestionType(self):
		return self.q_Type
	
	def getAnswerIDs(self):
		return self.a_IDs
	
	def getAnswerTexts(self):
		return self.a_Texts

	def getAnswerImages(self):
		return self.a_Images